package com.example.android.unscramble.ui

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import com.example.android.unscramble.data.allWords
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import kotlinx.coroutines.flow.update

/*El modelo es el "paquete" con todos los datos que se
pasan desde el viewmodel (intermediario entre datos y UI)
y la interfaz (parte que se ve) */

/* También es el modelo el que tiene todas las funciones para
procesar los datos */

class GameViewModel : ViewModel() {
    var userGuess by mutableStateOf("")
        private set

    private val _uiState = MutableStateFlow(GameUiState())
    //De esta manera se consigue que los datos sean sólo de lectura
    val uiState: StateFlow<GameUiState> = _uiState.asStateFlow()

    private lateinit var choosenWord: String

    //Diferencia con MutableListOf<String>¿?
    private var usedWords: MutableSet<String> = mutableSetOf()

    private fun pickRandomShuffledWord(): String{
        choosenWord = allWords.random()

        if(!usedWords.contains(choosenWord)){
            usedWords.add(choosenWord)
            return shuffleWord(choosenWord)
        } else return pickRandomShuffledWord()
    }

    private fun shuffleWord(word: String): String{
        val wordToChar = word.toCharArray()
        wordToChar.shuffle()

        while (String(wordToChar).equals(word)) wordToChar.shuffle()

        return String(wordToChar)
    }

    fun resetGame(){
        usedWords.clear()
        _uiState.value = GameUiState(currentShuffledWord = pickRandomShuffledWord())
    }

    init {
        resetGame()
    }

    fun updateUserGuess(guessedWord: String){
        userGuess = guessedWord
    }


    fun checkUserGuess(){
        if (userGuess.equals(choosenWord, ignoreCase = true)) {
        } else {
            // User's guess is wrong, show an error
            _uiState.update { currentState ->
                currentState.copy(isGuessedWordWrong = true)
            }
        }
    }

}