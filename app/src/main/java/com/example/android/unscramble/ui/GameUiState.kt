package com.example.android.unscramble.ui

//Lo que se envía dentro del modelo
data class GameUiState (
    val currentShuffledWord : String = "",
    val isGuessedWordWrong: Boolean = false
        )
